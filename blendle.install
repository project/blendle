<?php
/**
 * @file
 * Install file for the Blendle module.
 */

/**
 * Implements hook_requirements().
 */
function blendle_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break during installation.
  $t = get_t();

  if ($phase == 'runtime') {
    $messages = array();

    $libraries = libraries_get_libraries();
    if (!isset($libraries['blendle-button'])) {
      $messages[] = $t('Please install the Blendle SDK. See README.txt for more information.');
    }

    $api_secret = variable_get('blendle_api_secret');
    if (empty($api_secret)) {
      $messages[] = $t('Please setup your Blendle API secret in settings.php.');
    }

    $settings_page_url = url('admin/config/system/blendle');

    $provider_uid = variable_get('blendle_provider_uid');
    if (empty($provider_uid)) {
      $messages[] = $t('Please setup your Blendle Provider UID on the <a href="!settings-page-url">settings page</a>.', array('!settings-page-url' => $settings_page_url));
    }

    $public_key = variable_get('blendle_public_key');
    if (empty($public_key)) {
      $messages[] = $t('Please setup your Blendle Public key on the <a href="!settings-page-url">settings page</a>.', array('!settings-page-url' => $settings_page_url));
    }

    if (!empty($messages)) {
      $requirements['blendle_configuration'] = array(
        'title' => $t('Blendle Installation'),
        'value' => $t('Incomplete'),
        'description' => count($messages) > 1 ? theme('item_list', array('items' => $messages)) : reset($messages),
        'severity' => REQUIREMENT_ERROR,
      );
    }
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function blendle_schema() {
  return array(
    'blendle' => array(
      'description' => 'The main table to hold the Blendle data.',
      'fields' => array(
        'bid' => array(
          'description' => 'The primary identifier.',
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'entity_type' => array(
          'description' => 'The entity type this data is attached to.',
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
        ),
        'entity_id' => array(
          'description' => 'The entity id this data is attached to.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'status' => array(
          'description' => 'Whether the Blendle Button is turned on or off.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ),
        'word_count' => array(
          'description' => 'The amount of words for this node.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'indexes' => array(
        'entity_id' => array('entity_id'),
        'blendle_status' => array('status'),
      ),
      'primary key' => array('bid'),
    ),
  );
}

/**
 * Implements hook_uninstall().
 */
function blendle_uninstall() {
  // Keep variables in alphabetic order for easier maintenance and patching.
  $variables = array(
    'blendle_is_production',
    'blendle_provider_uid',
    'blendle_public_key',
  );

  $types = node_type_get_types();
  foreach ($types as $type) {
    $type_name = $type->type;
    $variables[] = "blendle_default_status_node_" . $type_name;
    $variables[] = "blendle_enable_node_" . $type_name;
  }

  foreach ($variables as $variable) {
    variable_del($variable);
  }
}

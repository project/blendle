<?php
/**
 * @file
 * Administration pages for the Blendle module.
 */

/**
 * Admin settings page.
 */
function blendle_admin_settings_page($form, &$form_state) {
  $form['blendle_provider_uid'] = array(
    '#type' => 'textfield',
    '#title' => t('Provider UID'),
    '#default_value' => variable_get('blendle_provider_uid', ''),
    '#description' => t('The provider UID as provided by Blendle.'),
  );

  $form['blendle_public_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Public key'),
    '#default_value' => variable_get('blendle_public_key', ''),
    '#description' => t('The public key as provided by Blendle.'),
  );

  $api_secret = variable_get('blendle_api_secret');
  $description = !empty($api_secret) ? t('API secret correctly setup in settings.php') : t('API secret missing. Please add the following line to your settings.php: !code', array('!code' => '<p><code>$conf[\'blendle_api_secret\'] = \'YOUR API SECRET\';</code></p>'));
  $form['blendle_api_secret'] = array(
    '#type' => 'item',
    '#title' => t('API secret'),
    '#markup' => $description,
  );

  $form['blendle_environment'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('Environment'),
  );
  $form['blendle_environment']['blendle_is_production'] = array(
    '#type' => 'checkbox',
    '#title' => t('Production'),
    '#default_value' => variable_get('blendle_is_production', ''),
    '#description' => t('Check this box to tell the Blendle API we are running on production'),
  );

  return system_settings_form($form);
}

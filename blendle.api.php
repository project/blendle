<?php

/**
 * @file
 * API documentation for the Blendle module.
 */

/**
 * Allows alteration of the excerpt that is shown to the user.
 *
 * @param array $excerpt
 *   Render array containing #markup.
 * @param object $node
 *   The node the user is trying to view.
 */
function hook_blendle_excerpt_alter(&$excerpt, $node) {
  $excerpt['#markup'] = 'Blendle - ' . $excerpt['#markup'];
}

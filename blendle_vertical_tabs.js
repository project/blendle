/**
 * @file
 * jQuery to provide summary information inside vertical tabs.
 */

(function ($) {

  "use strict";

  /**
   * Provide summary information for vertical tabs.
   */
  Drupal.behaviors.blendle_settings = {
    attach: function (context) {

      // Add the theme name as an additional class to the vertical-tabs div. This
      // can then be used to rectify the style for collapsible
      // fieldsets where different themes need slightly different fixes. The theme
      // is available in ajaxPageState.
      var theme = Drupal.settings.ajaxPageState['theme'];
      $('div.vertical-tabs').addClass(theme);

      // Provide summary when editing a node.
      $('fieldset#edit-blendle-settings', context).drupalSetSummary(function (context) {
        var vals = [];
        if ($('#edit-blendle-status').is(':checked')) {
          vals.push(Drupal.t('Enabled'));
        }
        else {
          vals.push(Drupal.t('Disabled'));
        }
        return vals.join('<br/>');
      });

      // Provide summary during content type configuration.
      $('fieldset#edit-blendle', context).drupalSetSummary(function (context) {
        var vals = [];
        if ($('#edit-blendle-enable-node', context).is(':checked')) {
          vals.push(Drupal.t('Enabled'));
        }
        else {
          vals.push(Drupal.t('Disabled'));
        }
        if ($('#edit-blendle-enable-node', context).is(':checked') && $('#edit-blendle-default-status-node', context).is(':checked')) {
          vals.push(Drupal.t('Default is <em>on</em> for new posts'));
        }
        return vals.join('<br/>');
      });
    }
  };

})(jQuery);

/**
 * @file
 * Re-attach Drupals behaviors after purchase.
 */
(function ($) {

  "use strict";

  Drupal.behaviors.blendle_behaviors = {
    attach: function (context) {
      // When Blendle returns the full content:
      window.BlendleButton.on('content', function (eventData) {
        if (eventData.user && eventData.user.status === 'ok') {
          // Re-attach the behaviors.
          Drupal.attachBehaviors();
        }
      });

      // The article contents is being removed from the document.
      // This could happen by a refund or logout of the user.
      window.BlendleButton.on('contentUnacquired', function (eventData) {
        if (eventData.item && eventData.item.status === 'ok') {
          // Re-attach the behaviors. We need to add a timeout to allow the page
          // to load, otherwise nothing will happen.
          setTimeout(Drupal.attachBehaviors, 2000);
        }
      });
    }
  };
})(jQuery);
